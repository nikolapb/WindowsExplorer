﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;


namespace WindowsFormsApplication
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //get a list of the drives
            string[] drives = Environment.GetLogicalDrives();

            foreach (string drive in drives)
            {
                DriveInfo di = new DriveInfo(drive);
                int driveImage;

                switch (di.DriveType)    //set the drive's icon
                {
                    case DriveType.CDRom:
                        driveImage = 3;
                        break;
                    case DriveType.Network:
                        driveImage = 6;
                        break;
                    case DriveType.NoRootDirectory:
                        driveImage = 8;
                        break;
                    case DriveType.Unknown:
                        driveImage = 8;
                        break;
                    default:
                        driveImage = 2;
                        break;
                }

                TreeNode node = new TreeNode(drive.Substring(0, 1), driveImage, driveImage)
                {
                    Tag = drive
                };

                if (di.IsReady == true)
                    node.Nodes.Add("...");

                dirsTreeView.Nodes.Add(node);
            }


        }

        private void dirsTreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Nodes.Count > 0)
            {
                if (e.Node.Nodes[0].Text == "..." && e.Node.Nodes[0].Tag == null)
                {
                    e.Node.Nodes.Clear();

                    //get the list of sub direcotires
                    string[] dirs = Directory.GetDirectories(e.Node.Tag.ToString());

                    //add files of rootdirectory
                    DirectoryInfo rootDir = new DirectoryInfo(e.Node.Tag.ToString());
                    foreach (var file in rootDir.GetFiles())
                    {
                        TreeNode n = new TreeNode(file.Name, 13, 13);
                        e.Node.Nodes.Add(n);
                    }

                    foreach (string dir in dirs)
                    {
                        DirectoryInfo di = new DirectoryInfo(dir);
                        TreeNode node = new TreeNode(di.Name, 0, 1);

                        try
                        {
                            //keep the directory's full path in the tag for use later
                            node.Tag = dir;

                            //if the directory has sub directories add the place holder
                            if (di.GetDirectories().Count() > 0)
                                node.Nodes.Add(null, "...", 0, 0);

                            foreach (var file in di.GetFiles())
                            {
                                TreeNode n = new TreeNode(file.Name, 13, 13);
                                node.Nodes.Add(n);
                            }

                        }
                        catch (UnauthorizedAccessException)
                        {
                            //display a locked folder icon
                            node.ImageIndex = 12;
                            node.SelectedImageIndex = 12;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "DirectoryLister",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            e.Node.Nodes.Add(node);
                        }
                    }
                }
            }
        }


        private void dirsTreeView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            StringBuilder filePath = new StringBuilder(e.Node.FullPath);

            if (filePath.Length < 3)
            {
                filePath.Insert(1, ":\\");

                setDriveDetails(filePath);
            }
            else
            {
                filePath.Insert(1, ":");
            }

            string path = filePath.ToString();


            Process.Start(path);


            //  ProcessStartInfo processStartInfo = new ProcessStartInfo(e.Node.Text);
            //  processStartInfo.UseShellExecute = true;
            //  Process.Start(processStartInfo);
        }

        private void dirsTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            textBoxDetails.Clear();

            StringBuilder filePath = new StringBuilder(e.Node.FullPath);


            if (filePath.Length < 3)
            {
                filePath.Insert(1, ":\\");

                setDriveDetails(filePath);      
            }
            else
            {
                filePath.Insert(1, ":");
            }

            string path = filePath.ToString();

            if (Directory.Exists(path) && path.Length > 3)
            {
                setDirectoryDetails(path);

            }
            else if (File.Exists(path))
            {
                setFileDetails(path);
            }
                        
        }

        private void setDriveDetails(StringBuilder filePath)
        {
            DriveInfo driveInfo = new DriveInfo(filePath.ToString());
            textBoxDetails.Text += "- Drive letter: " + filePath[0].ToString() + Environment.NewLine;
            textBoxDetails.Text += "- Drive name: " + driveInfo.Name + Environment.NewLine;
            textBoxDetails.Text += "- Free space: " + driveInfo.TotalFreeSpace + "B" + Environment.NewLine;
            textBoxDetails.Text += "- Total size: " + driveInfo.TotalSize + "B" + Environment.NewLine;
        }

        private void setDirectoryDetails(string path)
        {

            DirectoryInfo directory = new DirectoryInfo(path);

            string archive ="-";
            string readOnly="-";
            string hidden="-";
            string system = "-";

            if((directory.Attributes & FileAttributes.Archive) == 0)
            {
                archive = "Archive";
            }
            if ((directory.Attributes & FileAttributes.Hidden) == 0)
            {
                hidden = "Hidden";
            }
            if ((directory.Attributes & FileAttributes.ReadOnly) == 0)
            {
                readOnly = "ReadOnly";
            }
            if ((directory.Attributes & FileAttributes.System) == 0)
            {
                system = "System";
            }

            // app is showing all files from the system including hidden files
            // app doesn't counting hidden folders and files
            // this block of code is in try/catch because it is throwning an exception 
            // in case of selecting files which access is denied 

            try
            {
                int dirCount = Directory.GetDirectories(path).Length;

                textBoxDetails.Text += "- Directory name: " + directory.Name + Environment.NewLine;
                textBoxDetails.Text += "- Number of files: " + dirCount + Environment.NewLine;
                textBoxDetails.Text += "- Folder attributes: ("+archive+"," + readOnly + "," + hidden + "," + system + ")" + Environment.NewLine;

            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine("Denied access to selected directory "+e.Data);
            }
        }

        private void setFileDetails(string path)
        {
            string archive = "-";
            string readOnly = "-";
            string hidden = "-";
            string system = "-";

            FileInfo file = new FileInfo(path);

            if ((file.Attributes & FileAttributes.Archive) == 0)
            {
                archive = "Archive";
            }
            if ((file.Attributes & FileAttributes.Hidden) == 0)
            {
                hidden = "Hidden";
            }
            if ((file.Attributes & FileAttributes.ReadOnly) == 0)
            {
                readOnly = "ReadOnly";
            }
            if ((file.Attributes & FileAttributes.System) == 0)
            {
                system = "System";
            }

            textBoxDetails.Text += "- File name: " + file.Name + Environment.NewLine;
            textBoxDetails.Text += "- File size: " + file.Length + "B" + Environment.NewLine;
            textBoxDetails.Text += "- File attributes: (" + archive + "," + readOnly + "," + hidden + "," + system + ")" + Environment.NewLine;

        }

    }
}
